/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FSM_H
#define FSM_H

#include <stdbool.h>

#define DEFAULT_COOL_TEMPERATURE (30)
#define MINIMUM_TIME_MIN (1)
#define MAXIMUM_TEMP (100)

struct fsm_signal {
	bool on;
	bool off;
	bool go;
	bool stop;
	bool preheat_done;
	bool time_done;
	float target_temperature;
	float cool_temperature;
	float current_temperature;
	int minutes_to_wait;
	int minutes_elapsed;
	bool time_over;
};

typedef struct fsm_signal fsm_signal_t;

void set_fsm_signal(fsm_signal_t);
fsm_signal_t get_fsm_signal();

void set_fsm_state(void (*st)());
void (*get_fsm_state())();
void next_state();

void st_off();
void st_on();
void st_preheat();
void st_preheat_1();
void st_heat();
void st_cooldown();

#endif
