/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MODBUS_H
#define MODBUS_H

#define STR(x) #x
#define XSTR(x) STR(x)

#define _STR_LEN 256U
#define _MAX_STR_LEN 255
#define _DATA_LEN 257U
#define _MSG_LEN 261U

#define STR_LEN (_STR_LEN)
#define MAX_STR_LEN (_MAX_STR_LEN)
#define DATA_LEN (_DATA_LEN)
#define MSG_LEN (_MSG_LEN)

#define STR_LEN_STR XSTR(_STR_LEN)
#define MAX_STR_LEN_STR XSTR(_MAX_STR_LEN)
#define DATA_LEN_STR XSTR(_DATA_LEN)
#define MSG_LEN_STR XSTR(_MSG_LEN)

#define ANSWER_LEN (9)

#include <stddef.h>
#include <stdint.h>

#define DEV_ADDR (0x01)

enum fun_codes {
	READ = 0x23,
	WRITE = 0x16
};

enum fun_read {
	READ_INT32 = 0xA1,
	READ_FLOAT = 0xA2,
	READ_STRING = 0xA3,
	READ_INTERNAL_TEMP_FLOAT = 0xC1,
	READ_REFERENCE_TEMP_FLOAT = 0xC2,
	READ_CMD_INT32 = 0xC3
};

enum fun_write {
	WRITE_INT32 = 0xB1,
	WRITE_FLOAT = 0xB2,
	WRITE_STRING = 0xB3,
	WRITE_CONTROL_INT32 = 0xD1,
	WRITE_REFERENCE_INT32 = 0xD2,
	WRITE_ON_OFF_INT32 = 0xD3,
	WRITE_TERMINAL_DASHBOARD_INT32 = 0xD4,
	WRITE_START_STOP_INT32 = 0xD5,
	WRITE_TIMER_INT32 = 0xD6
};

enum user_commands {
	CMD_ON = 0x01,
	CMD_OFF = 0x02,
	CMD_START = 0x03,
	CMD_STOP = 0x04,
	CMD_ADD_TIME = 0x05,
	CMD_SUB_TIME = 0x06,
	CMD_MENU = 0x07
};

struct rtu_message {
	uint8_t dev_address;
	uint8_t fun_code;
	size_t data_len;
	uint8_t data[DATA_LEN];
	uint16_t crc;
};

struct serial_message {
	uint8_t msg[MSG_LEN];
	size_t len;
};

enum msg_data_type {
	INT32 = 0,
	FLOAT,
	STRING
};

union msg_data {
	int32_t i32;
	float r32;
	uint8_t str[STR_LEN];
};

struct extracted_msg_data {
	enum msg_data_type type;
	size_t len;
	union msg_data data;
};

typedef struct rtu_message rtu_message_t;
typedef struct serial_message serial_message_t;
typedef struct extracted_msg_data extracted_msg_data_t;

int serialise(rtu_message_t *msg, serial_message_t *ser_msg);
int deserialise(const serial_message_t ser_msg, rtu_message_t *msg);

rtu_message_t read_int32(uint8_t dev);
rtu_message_t read_float(uint8_t dev);
rtu_message_t read_string(uint8_t dev);

rtu_message_t write_int32(uint8_t dev, int32_t to_send);
rtu_message_t write_float(uint8_t dev, float to_send);
rtu_message_t write_string(uint8_t dev, uint8_t *to_send, size_t len);

rtu_message_t ask_internal_temperature(uint8_t dev);
rtu_message_t ask_reference_temperature(uint8_t dev);
rtu_message_t ask_user_commands(uint8_t dev);

rtu_message_t send_control_signal(uint8_t dev, int32_t signal);
rtu_message_t send_reference_signal(uint8_t dev, int32_t signal);
rtu_message_t send_on_off_status(uint8_t dev, int32_t status);
rtu_message_t send_dashboard_terminal_status(uint8_t dev, int32_t status);
rtu_message_t send_start_stop_status(uint8_t dev, int32_t status);
rtu_message_t send_timer(uint8_t dev, int32_t time);

int get_message_data(rtu_message_t msg, extracted_msg_data_t *data);
void print_message_data(extracted_msg_data_t data);

#endif
