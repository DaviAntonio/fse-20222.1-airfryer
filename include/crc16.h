/*
 * crc16.h
 *
 *  Created on: 18/03/2014
 *      Author: Renato Coral Sampaio
 *  Modified by: Davi Antônio da Silva Santos on 23/08/2022
 */

#ifndef CRC16_H_
#define CRC16_H_

#include <stdint.h>
#include <stddef.h>

uint16_t CRC16(uint16_t crc, uint8_t data);
uint16_t calcula_CRC(const uint8_t *commands, size_t size);

#endif /* CRC16_H_ */
