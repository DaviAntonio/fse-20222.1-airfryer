/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIME_UTILS_H
#define TIME_UTILS_H

#include <time.h> // struct timespec

void timespec_diff(struct timespec *a, struct timespec *b,
		struct timespec *result);
void timespec_add(struct timespec *a, struct timespec *b,
		struct timespec *result);
int timespec2ms(struct timespec t);
int timespec2min(struct timespec t);
struct timespec ms2timespec(int ms);

#endif
