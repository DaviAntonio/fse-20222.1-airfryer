/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <inttypes.h> // printf
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <error.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <time.h>
#include <signal.h>

#include <wiringPi.h>
#include <softPwm.h>

#include "modbus.h"
#include "uart.h"
#include "fsm.h"
#include "pid.h"
#include "time_utils.h"
#include "control_lcd_16x2.h"
#include "bme280_userspace.h"

#define MAX_EVENTS (100)

#define _MENU_ITEM_DESC_LEN 15
#define MENU_ITEM_DESC_LEN (_MENU_ITEM_DESC_LEN)

#define PID_KP (30.0)
#define PID_KI (0.2)
#define PID_KD (400.0)

#define FAN_DEADZONE (40.0)

#define STR_TIME_LEN (100)
#define TIMER_RESOLUTION_MS (200)
#define PID_TIMER_MS (1000)

#define GPIO_HEATER_BCM (23)
#define GPIO_FAN_BCM (24)

struct menu_item {
	char desc[MENU_ITEM_DESC_LEN];
	int time_mins;
	float temp;
};

typedef struct menu_item menu_item_t;

static const menu_item_t MENU_ITEMS[] = {
	{
		.desc = "CUSTOM",
		.time_mins = 1,
		.temp = 50,
	},
	{
		.desc = "PASTEL VENTO",
		.time_mins = 2,
		.temp = 60,
	},
	{
		.desc = "FRANGO RU",
		.time_mins = 2,
		.temp = 65
	}
};

struct epoll_event events[MAX_EVENTS];

rtu_message_t tx_msg;
rtu_message_t rx_msg;

int epoll_fd;

void uart_loop(int, FILE *, bool);

void print_options();

void sigint_handler(int caught_signal);

void sigint_handler(int caught_signal)
{
	printf("Caught %d\n", caught_signal);
	printf("Stopping\n");

	softPwmWrite(GPIO_HEATER_BCM, 0);
	softPwmWrite(GPIO_FAN_BCM, 0);

	exit(0);
}

void print_options()
{
	printf("10. Set target temp\n");
	printf("11. Set minutes\n");
	printf("12. Go\n");
	printf("13. Stop\n");
	printf("14. On\n");
	printf("15. Off\n");
	printf("16. Menu\n");
	printf("20. Force internal temperature\n");
	printf("21. Force environment temperature\n");
}

void uart_loop(int uart0_fd, FILE *log_fp, bool uart_temp_time)
{
	int ready_fds;
	bool stop = false;
	bool can_read = false;
	bool can_write = false;
	fsm_signal_t fsig;

	struct timespec before, after, ts_diff;
	int ms_diff;
	int pid_timer = 0;

	const int PID_TIMER_COUNTS = PID_TIMER_MS / TIMER_RESOLUTION_MS;

	printf("TIMER_RESOLUTION_MS = %d\n", TIMER_RESOLUTION_MS);
	printf("PID_TIMER_MS = %d\n", PID_TIMER_MS);
	printf("PID_TIMER_COUNTS = %d\n", PID_TIMER_COUNTS);

	rtu_message_t (*ask_msgs[])(uint8_t) = {
		ask_internal_temperature,
		ask_reference_temperature,
		ask_user_commands
	};
	int ask_msg_counter = 0;
	const int ASK_MSGS_COUNTS = sizeof(ask_msgs) / sizeof(ask_msgs[0]);

	rtu_message_t (*send_msgs[])(uint8_t, int32_t) = {
		send_control_signal,
		send_reference_signal,
		send_on_off_status,
		//send_dashboard_terminal_status,
		send_start_stop_status,
		send_timer
	};
	int send_msg_counter = 0;
	const int SEND_MSGS_COUNTS = sizeof(send_msgs) / sizeof(send_msgs[0]);

	bool hit_menu = false;
	int menu_counter = 0;
	const int MENU_COUNTS = sizeof(MENU_ITEMS) / sizeof(MENU_ITEMS[0]);

	int tx_msgs_counter = 0;
	const int TX_MSGS_COUNTS = ASK_MSGS_COUNTS + SEND_MSGS_COUNTS;

	double control = 0.0;

	bool on = false;
	bool going = false;

	while (!stop) {
		// block forever until event or signal
		clock_gettime(CLOCK_BOOTTIME, &before);
		ready_fds = epoll_wait(epoll_fd, events, MAX_EVENTS,
				TIMER_RESOLUTION_MS);
		can_read = false;
		can_write = false;

		if (ready_fds == 0) {
			//printf("Timed out (%u ms)...\n", TIMER_RESOLUTION_MS);
		} else {
			for (int i = 0; i < ready_fds; i++) {
				int found_fd = events[i].data.fd;
				uint32_t found_event = events[i].events;

				if (found_fd == STDIN_FILENO) {
					// stdin
					if (found_event & EPOLLIN) {
						// read
						int option;

						print_options();
						printf("Type option:\n");
						scanf(" %d", &option);

						int target_temp = 0;
						int minutes_to_wait = 1;
						int forced_internal_temp;
						int forced_env_temp;

						switch (option) {
						case 10:
							fsig = get_fsm_signal();

							printf("Type target temp: ");
							scanf(" %d", &target_temp);

							fsig.target_temperature = target_temp;
							set_fsm_signal(fsig);
							break;
						case 11:
							fsig = get_fsm_signal();

							printf("Type minutes to wait: ");
							scanf(" %d", &minutes_to_wait);

							fsig.minutes_to_wait = minutes_to_wait;
							set_fsm_signal(fsig);
							break;
						case 12:
							fsig = get_fsm_signal();
							fsig.go = true;
							set_fsm_signal(fsig);
							break;
						case 13:
							fsig = get_fsm_signal();
							fsig.stop = true;
							set_fsm_signal(fsig);
							break;
						case 14:
							fsig = get_fsm_signal();
							fsig.on = true;
							set_fsm_signal(fsig);
							break;
						case 15:
							fsig = get_fsm_signal();
							fsig.off = true;
							set_fsm_signal(fsig);
							break;
						case 16:
							hit_menu = true;
							break;
						case 20:
							fsig = get_fsm_signal();

							printf("Force internal temperature: ");
							scanf(" %d", &forced_internal_temp);

							fsig.current_temperature = forced_internal_temp;
							set_fsm_signal(fsig);
							break;
						case 21:
							fsig = get_fsm_signal();

							printf("Force external temperature: ");
							scanf(" %d", &forced_env_temp);

							fsig.cool_temperature = forced_env_temp;
							set_fsm_signal(fsig);
							break;
						default:
							printf("Unknown option %d\n", option);
						}
					} else {
						// unknown event
					}
				}
				if (found_fd == uart0_fd) {
					// uart0
					if (found_event & EPOLLIN) {
						// can read
						can_read = true;
					} else if (found_event & EPOLLOUT) {
						// can write
						can_write = true;
					} else {
						// unknown event
					}
				}
			}
		}

		clock_gettime(CLOCK_BOOTTIME, &after);
		timespec_diff(&after, &before, &ts_diff);
		ms_diff = timespec2ms(ts_diff);

		if (TIMER_RESOLUTION_MS > ms_diff) {
			struct timespec extra = ms2timespec(TIMER_RESOLUTION_MS - ms_diff);
			printf("Must sleep extra %d ms\n", TIMER_RESOLUTION_MS - ms_diff);
			nanosleep(&extra, NULL);
		}

		if (can_read) {
			serial_message_t ser_msg;
			memset(&ser_msg, 0, sizeof(ser_msg));
			printf("Reading UART\n");
			ser_msg.len = read(uart0_fd, ser_msg.msg, MSG_LEN);
			printf("Got message with %zu bytes\n", ser_msg.len);

			printf("Raw message: ");
			for (size_t i = 0; i < ser_msg.len; i++) {
				printf("0x%02" PRIX8 " ", ser_msg.msg[i]);
			}

			int rounds = ser_msg.len / ANSWER_LEN;
			printf("Try reading %d messages\n", rounds);

			for (int i = 0; i < rounds; i++) {
				int result;
				serial_message_t t_ser_msg;

				memcpy(t_ser_msg.msg, &(ser_msg.msg[i * ANSWER_LEN]), ANSWER_LEN);
				t_ser_msg.len = ANSWER_LEN;

				printf("Deserialising\n");
				result = deserialise(t_ser_msg, &rx_msg);

				if (result == 0) {
					printf("Seems OK\n");
					extracted_msg_data_t data;
					result = get_message_data(rx_msg, &data);
					if (result == 0) {
						print_message_data(data);
						switch (rx_msg.data[0]) {
						case READ_INTERNAL_TEMP_FLOAT:
							fsig = get_fsm_signal();
							fsig.current_temperature = data.data.r32;
							set_fsm_signal(fsig);
							break;
						case READ_REFERENCE_TEMP_FLOAT:
							if (!uart_temp_time) {
								printf("UART TR OFF!\n");
							} else {
								fsig = get_fsm_signal();
								fsig.target_temperature = data.data.r32;
								set_fsm_signal(fsig);
							}
							break;
						case READ_CMD_INT32:
							fsig = get_fsm_signal();
							switch (data.data.i32) {
							case CMD_ON:
								printf("CMD_ON\n");
								fsig.on = true;
								break;
							case CMD_OFF:
								printf("CMD_OFF\n");
								fsig.off = true;
								break;
							case CMD_START:
								printf("CMD_START\n");
								fsig.go = true;
								break;
							case CMD_STOP:
								printf("CMD_STOP\n");
								fsig.stop = true;
								break;
							case CMD_ADD_TIME:
								if (!uart_temp_time) {
									printf("UART TT+ OFF!\n");
								} else {
									printf("TT+\n");
									fsig.minutes_to_wait++;
									printf("TT: %d\n", fsig.minutes_to_wait);
								}
								break;
							case CMD_SUB_TIME:
								if (!uart_temp_time) {
									printf("UART TT- OFF!\n");
								} else {
									printf("TT-\n");
									fsig.minutes_to_wait--;
									printf("TT: %d\n", fsig.minutes_to_wait);
								}
								break;
							case CMD_MENU:
								printf("CMD_MENU\n");
								hit_menu = true;
								break;
							default:
								printf("Unknown user command 0x%02X\n",
										data.data.i32);
							}
							set_fsm_signal(fsig);
							break;
						default:
							printf("Unknown subfunction 0x%02X\n",
									rx_msg.data[0]);
						}
					} else {
						printf("Broken message data (error %d)\n",
								result);
					}
				} else {
					printf("Invalid message (error %d)\n", result);
				}
			}
			//printf("discard unread\n");
			//discard_unread(uart0_fd);
		}

		pid_timer = (pid_timer + 1) % PID_TIMER_COUNTS;
		tx_msgs_counter = (tx_msgs_counter + 1) % TX_MSGS_COUNTS;

		if (tx_msgs_counter >= 0 && tx_msgs_counter < ASK_MSGS_COUNTS) {
			ask_msg_counter = (ask_msg_counter + 1) % ASK_MSGS_COUNTS;
			tx_msg = ask_msgs[ask_msg_counter](DEV_ADDR);
		} else {
			send_msg_counter = (send_msg_counter + 1) % SEND_MSGS_COUNTS;
			tx_msg = send_msgs[send_msg_counter](DEV_ADDR, (int) control);
			switch (send_msg_counter) {
			case 0:
				tx_msg = send_msgs[send_msg_counter](DEV_ADDR, (int) control);
				break;
			case 1:
				tx_msg = send_msgs[send_msg_counter](DEV_ADDR, fsig.target_temperature);
				break;
			case 2:
				tx_msg = send_msgs[send_msg_counter](DEV_ADDR, on);
				break;
			/*case 3:
				tx_msg = send_msgs[send_msg_counter](DEV_ADDR, !uart_temp_time);
				break;*/
			case 3:
				tx_msg = send_msgs[send_msg_counter](DEV_ADDR, going);
				break;
			case 4:
				tx_msg = send_msgs[send_msg_counter](DEV_ADDR, fsig.minutes_to_wait);
				break;
			default:
				printf("BORK: send_msg_counter = %d!\n", send_msg_counter);
			}
		}

		if (can_write) {
			int result;
			uint8_t i_sent = 0;

			serial_message_t ser_msg;
			result = serialise(&tx_msg, &ser_msg);

			if (result == 0) {
				i_sent = write(uart0_fd, &ser_msg.msg, ser_msg.len);
				wait_write(uart0_fd);

				if (ser_msg.len == i_sent) {
					printf("Successfully sent %zu bytes\n", ser_msg.len);
				} else {
					printf("Sent only %d/%zu bytes\n", i_sent, ser_msg.len);
				}
			}
		}

		if (hit_menu) {
			hit_menu = false;
			menu_counter = (menu_counter + 1) % MENU_COUNTS;
		}
		menu_item_t choosen_item = MENU_ITEMS[menu_counter];

		if (pid_timer == 0) {
			printf("%d ms elapsed\n", PID_TIMER_MS);

			next_state();

			float setpoint;
			double process;
			unsigned int fan = 0;
			unsigned int heater = 0;
			int rslt = 0;
			void (*fstate)();
			char lcd_line_0[LCD_LINE_LEN] = {0};
			char lcd_line_1[LCD_LINE_LEN] = {0};

			fstate = get_fsm_state();
			fsig = get_fsm_signal();

			float external_temp = fsig.cool_temperature;

			printf("Try to read external temp\n");
			rslt = get_temperature(&external_temp);

			if (rslt != 0) {
				external_temp = fsig.cool_temperature;
			} else {
				fsig.cool_temperature = external_temp;
				set_fsm_signal(fsig);
			}

			fsig = get_fsm_signal();

			if (fstate != st_off) {
				on = true;
			} else {
				on = false;
			}

			if (fstate == st_on) {
				if (menu_counter != 0) {
					fsig.target_temperature = choosen_item.temp;
					fsig.minutes_to_wait = choosen_item.time_mins;
				}
				set_fsm_signal(fsig);
				fsig = get_fsm_signal();

				printf("MENU: %s TR %d MIN %d\n",
						choosen_item.desc,
						(int) fsig.target_temperature,
						fsig.minutes_to_wait);
				snprintf(lcd_line_0, LCD_LINE_LEN, "%.*s",
						MENU_ITEM_DESC_LEN,
						choosen_item.desc);
				snprintf(lcd_line_1, LCD_LINE_LEN,
						"TR %.1f TM %d",
						fsig.target_temperature,
						fsig.minutes_to_wait);
				ClrLcd();
				lcdLoc(LINE1);
				typeln(lcd_line_0);
				lcdLoc(LINE2);
				typeln(lcd_line_1);
			} else if (fstate == st_preheat) {
				snprintf(lcd_line_0, LCD_LINE_LEN,
						"PREHEATING");
				snprintf(lcd_line_1, LCD_LINE_LEN,
						"TR %.1f TC %.1f",
						fsig.target_temperature,
						fsig.current_temperature);
				ClrLcd();
				lcdLoc(LINE1);
				typeln(lcd_line_0);
				lcdLoc(LINE2);
				typeln(lcd_line_1);
			} else if (fstate == st_heat) {
				snprintf(lcd_line_0, LCD_LINE_LEN,
						"HOT TE %d",
						fsig.minutes_elapsed);
				snprintf(lcd_line_1, LCD_LINE_LEN,
						"TM %d TC %.1f",
						fsig.minutes_to_wait,
						fsig.current_temperature);
				ClrLcd();
				lcdLoc(LINE1);
				typeln(lcd_line_0);
				lcdLoc(LINE2);
				typeln(lcd_line_1);
			} else if (fstate == st_cooldown) {
				snprintf(lcd_line_0, LCD_LINE_LEN,
						"COOLDOWN");
				snprintf(lcd_line_1, LCD_LINE_LEN,
						"T0 %.1f TC %.1f",
						fsig.cool_temperature,
						fsig.current_temperature);
				ClrLcd();
				lcdLoc(LINE1);
				typeln(lcd_line_0);
				lcdLoc(LINE2);
				typeln(lcd_line_1);
			} else {
				ClrLcd();
			}

			going = false;
			if ((fstate == st_preheat) || (fstate == st_preheat_1)
					|| (fstate == st_heat)) {
				printf("Run PID Loop\n");
				going = true;
				setpoint = fsig.target_temperature;
				process = fsig.current_temperature;

				pid_atualiza_referencia(setpoint);
				control = pid_controle(process);

				if (control < 0.0) {
					fan = (-control < FAN_DEADZONE) ?
						FAN_DEADZONE : -control;
					heater = 0;
					softPwmWrite(GPIO_FAN_BCM, fan);
				} else {
					fan = 0;
					heater = control;
					softPwmWrite(GPIO_HEATER_BCM, heater);
				}
			} else if (fstate == st_cooldown) {
				printf("Cooldown\n");
				control = -100.0;
				heater = 0;
				fan = 100;

				softPwmWrite(GPIO_FAN_BCM, fan);
				softPwmWrite(GPIO_HEATER_BCM, heater);
			} else {
				control = 0;
				heater = 0;
				fan = 0;

				softPwmWrite(GPIO_FAN_BCM, fan);
				softPwmWrite(GPIO_HEATER_BCM, heater);
			}

			printf("Heater %u %% Fan %u %%\n", heater, fan);

			struct timespec timestamp;
			struct tm *cal_time;
			char time_str[STR_TIME_LEN];

			memset(time_str, 0, STR_TIME_LEN);

			clock_gettime(CLOCK_REALTIME, &timestamp);
			cal_time = localtime(&timestamp.tv_sec);
			strftime(time_str, STR_TIME_LEN, "%FT%H:%M:%S%z", cal_time);

			fprintf(log_fp, "%s,%f,%f,%f,%u,%u\n",
					time_str,
					fsig.current_temperature,
					fsig.cool_temperature,
					fsig.target_temperature,
					heater,
					fan);
			fflush(log_fp);
			printf("Log: %s,%f,%f,%f,%u,%u\n",
					time_str,
					fsig.current_temperature,
					fsig.cool_temperature,
					fsig.target_temperature,
					heater,
					fan);
		}
	}
}

int main()
{
	int uart0_fd;
	int result;
	fsm_signal_t fsm_s;
	FILE *log_fp;

	int option = 3;
	bool uart_temp_time = true;
	bool stdin_temp_time = true;

	struct sigaction s_action;

	printf("Choose inputs\n");
	printf("1. UART only\n");
	printf("2. STDIN only\n");
	printf("*. Both\n");
	scanf(" %d", &option);

	switch (option) {
	case 1:
		uart_temp_time = true;
		stdin_temp_time = false;
		printf("UART ONLY SELECTED\n");
		break;
	case 2:
		uart_temp_time = false;
		stdin_temp_time = true;
		printf("STDIN ONLY SELECTED\n");
		break;
	default:
		uart_temp_time = true;
		stdin_temp_time = true;
		printf("UART AND STDIN SELECTED\n");
	}

	uart0_fd = open_uart("/dev/serial0");

	if (uart0_fd == -1) {
		fprintf(stderr, "Failed to open uart\n");
		return -1;
	} else {
		printf("Uart opened\n");
	}

	result = setup_uart(uart0_fd);
	if (result != 0) {
		fprintf(stderr, "Failed to setup UART\n");
		return -2;
	}

	// Initialise epoll
	epoll_fd = epoll_create1(0);
	if (epoll_fd == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"epoll_create1()");
	}

	struct epoll_event ev;

	// watch uart0
	ev.data.fd = uart0_fd;
	ev.events = EPOLLIN | EPOLLOUT;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, uart0_fd, &ev);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "epoll_ctl()");
	}

	// watch stdin input
	if (stdin_temp_time) {
		ev.data.fd = STDIN_FILENO;
		ev.events = EPOLLIN;
		result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, STDIN_FILENO, &ev);
		if (result == -1) {
			error_at_line(-1, errno, __FILE__, __LINE__,
					"epoll_ctl()");
		}
	}

	// setup FSM
	set_fsm_state(st_off);
	memset(&fsm_s, 0, sizeof(fsm_s));
	fsm_s.cool_temperature = DEFAULT_COOL_TEMPERATURE;
	set_fsm_signal(fsm_s);

	// setup PID loop
	pid_configura_constantes(PID_KP, PID_KI, PID_KD);

	// log file
	log_fp = fopen("airfryer.log", "w");
	fprintf(log_fp, "%s,%s,%s,%s,%s,%s\n",
			"timestamp", "internal_temp", "external_temp",
			"target_temp", "heater_pwm", "fan_pwm");
	fflush(log_fp);

	// setup WiringPi with BCM numbers
	printf("Setup WiringPi\n");
	wiringPiSetupGpio();

	result = softPwmCreate(GPIO_HEATER_BCM, 0, 100);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"softPwmCreate()");
	}

	result = softPwmCreate(GPIO_FAN_BCM, 0, 100);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"softPwmCreate()");
	}

	// prepare lcd
	lcd_init();

	// prepare signal handler
	memset(&s_action, 0, sizeof(s_action));
	s_action.sa_handler = sigint_handler;
	sigaction(SIGINT, &s_action, NULL);

	// prepare BME280
	prepare_sensor();

	uart_loop(uart0_fd, log_fp, uart_temp_time);

	close(uart0_fd);
	fclose(log_fp);

	return 0;
}
