/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "fsm.h"
#include "time_utils.h"

static void (*state)();
static fsm_signal_t fsm_s;

static struct timespec last_time;
static struct timespec elapsed_time;

void set_fsm_signal(fsm_signal_t s)
{
	fsm_s = s;

	fsm_s.target_temperature =
		(fsm_s.target_temperature < fsm_s.cool_temperature) ?
		fsm_s.cool_temperature : fsm_s.target_temperature;
	fsm_s.target_temperature =
		(fsm_s.target_temperature > MAXIMUM_TEMP) ?
		MAXIMUM_TEMP : fsm_s.target_temperature;

	fsm_s.minutes_to_wait = (fsm_s.minutes_to_wait < MINIMUM_TIME_MIN) ?
		MINIMUM_TIME_MIN : fsm_s.minutes_to_wait;

	printf("Current signals\n");
	printf("on = %d\n", fsm_s.on);
	printf("off = %d\n", fsm_s.off);
	printf("go = %d\n", fsm_s.go);
	printf("stop = %d\n", fsm_s.stop);
	printf("preheat_done = %d\n", fsm_s.preheat_done);
	printf("time_done = %d\n", fsm_s.time_done);
	printf("target_temperature = %f\n", fsm_s.target_temperature);
	printf("cool_temperature = %f\n", fsm_s.cool_temperature);
	printf("current_temperature = %f\n", fsm_s.current_temperature);
	printf("minutes_to_wait = %d\n", fsm_s.minutes_to_wait);
	printf("time_over = %d\n", fsm_s.time_over);
}

fsm_signal_t get_fsm_signal()
{
	return fsm_s;
}

void set_fsm_state(void (*st)())
{
	state = st;
}

void (*get_fsm_state())()
{
	return state;
}

void next_state()
{
	(*state)();
}

void st_off()
{
	printf("st_off\n");

	if (fsm_s.on == true) {
		printf("on = true\n");
		fsm_s.on = false;
		state = st_on;
		printf("Go to st_on\n");
	} else {
		state = st_off;
		printf("Go to st_off\n");
	}
}

void st_on()
{
	printf("st_on\n");

	if (fsm_s.off == true) {
		printf("off = true\n");
		fsm_s.off = false;
		state = st_off;
		printf("Go to st_off\n");
	} else if (fsm_s.go == true) {
		printf("go = true\n");
		fsm_s.go = false;
		state = st_preheat;
		printf("Go to st_preheat\n");
	} else {
		state = st_on;
		printf("Go st_on\n");
	}
}

void st_preheat()
{
	printf("st_preheat\n");

	if (fsm_s.off == true) {
		printf("off = true\n");
		fsm_s.off = false;
		state = st_off;
		printf("Go to st_off\n");
	} else if (fsm_s.stop == true) {
		printf("stop = true\n");
		fsm_s.stop = false;
		state = st_on;
		printf("Go to st_on\n");
	} else if (fsm_s.current_temperature >= fsm_s.target_temperature) {
		printf("curr_temp  %f >= target %f\n", fsm_s.current_temperature,
				fsm_s.target_temperature);
		state = st_preheat_1;
		printf("Go to st_preheat_1\n");
	} else {
		state = st_preheat;
		printf("Go to st_preheat\n");
	}
}

void st_preheat_1()
{
	clock_gettime(CLOCK_BOOTTIME, &last_time);
	memset(&elapsed_time, 0, sizeof(elapsed_time));
	fsm_s.minutes_elapsed = 0;
	state = st_heat;
	printf("Go to st_heat\n");
}

void st_heat()
{
	printf("st_heat\n");

	struct timespec curr_time;
	struct timespec interval;

	clock_gettime(CLOCK_BOOTTIME, &curr_time);

	timespec_diff(&curr_time, &last_time, &interval);
	timespec_add(&elapsed_time, &interval, &elapsed_time);
	fsm_s.minutes_elapsed = timespec2min(elapsed_time);

	last_time = curr_time;

	printf("%ld.%09ld s since last observation\n", interval.tv_sec,
			interval.tv_nsec);
	printf("%ld.%09ld s elapsed\n", elapsed_time.tv_sec,
			elapsed_time.tv_nsec);
	printf("Minutes elapsed: %d mins\n", fsm_s.minutes_elapsed);

	if (fsm_s.off == true) {
		printf("off = true\n");
		fsm_s.off = false;
		state = st_off;
		printf("Go to st_off\n");
	} else if (fsm_s.stop == true) {
		printf("stop = true\n");
		fsm_s.stop = false;
		state = st_on;
		printf("Go to st_on\n");
	} else if ((fsm_s.time_over == true)
			|| (fsm_s.minutes_elapsed >= fsm_s.minutes_to_wait)) {
		printf("time_over = true\n");
		printf("elapsed %d >= to_wait %d\n", fsm_s.minutes_elapsed,
				fsm_s.minutes_to_wait);
		fsm_s.time_over = false;
		state = st_cooldown;
		printf("time over\n");
	} else {
		state = st_heat;
		printf("Go to st_heat\n");
	}
}

void st_cooldown()
{
	printf("st_cooldown\n");

	if (fsm_s.off == true) {
		printf("off = true\n");
		fsm_s.off = false;
		state = st_off;
		printf("Go to st_off\n");
	} else if (fsm_s.stop == true) {
		printf("stop = true\n");
		fsm_s.stop = false;
		state = st_on;
		printf("Go to st_on\n");
	} else if (fsm_s.current_temperature <= fsm_s.cool_temperature + 0.5f) {
		printf("current_temp %f <= cool_temp %f\n", fsm_s.current_temperature,
				fsm_s.cool_temperature);
		state = st_off;
		printf("Go to st_off\n");
	} else {
		state = st_cooldown;
		printf("Go to st_cooldown\n");
	}
}
