/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <unistd.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "uart.h"

int open_uart(char *path)
{
	int uart0_fd;

	uart0_fd = open(path, O_RDWR | O_NOCTTY);

	if (uart0_fd == -1)
		perror(NULL);

	return uart0_fd;
}

int setup_uart(int fd)
{
	struct termios options;
	int result;

	tcgetattr(fd, &options);

	options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;

	result = tcflush(fd, TCIOFLUSH);
	if (result == -1) {
		perror(NULL);
		return -1;
	}

	result = tcsetattr(fd, TCSANOW, &options);
	if (result == -1) {
		perror(NULL);
		return -2;
	}

	return 0;
}

void wait_write(int fd)
{
	int result;
	result = tcdrain(fd);

	if (result == -1) {
		perror(NULL);
	}
}

void discard_unread(int fd)
{
	int result;
	result = tcflush(fd, TCIFLUSH);

	if (result == -1) {
		perror(NULL);
	}
}
