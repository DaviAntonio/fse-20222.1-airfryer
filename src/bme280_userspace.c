/**
* Copyright (c) 2020 Bosch Sensortec GmbH. All rights reserved.
* Copyright (c) 2022 Davi Antônio da Silva Santos. All rights reserved.
*
* BSD-3-Clause
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from
*    this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* @file       bme280_userspace.c
*
*/

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

/******************************************************************************/
/*!                         System header files                               */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

/******************************************************************************/
/*!                         Own header files                                  */
#include "bme280.h"
#include "bme280_userspace.h"

struct bme280_dev gdev;
struct identifier gid;

/*!
 * @brief This function reading the sensor's registers through I2C bus.
 */
int8_t user_i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr)
{
    struct identifier id;

    id = *((struct identifier *)intf_ptr);

    write(id.fd, &reg_addr, 1);
    read(id.fd, data, len);

    return 0;
}

/*!
 * @brief This function provides the delay for required time (Microseconds) as per the input provided in some of the
 * APIs
 */
void user_delay_us(uint32_t period, void *intf_ptr)
{
    usleep(period);
}

/*!
 * @brief This function for writing the sensor's registers through I2C bus.
 */
int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr)
{
    uint8_t *buf;
    struct identifier id;

    id = *((struct identifier *)intf_ptr);

    buf = malloc(len + 1);
    buf[0] = reg_addr;
    memcpy(buf + 1, data, len);
    if (write(id.fd, buf, len + 1) < (uint16_t)len)
    {
        return BME280_E_COMM_FAIL;
    }

    free(buf);

    return BME280_OK;
}

/*!
 * @brief This API used to print the sensor temperature, pressure and humidity data.
 */
void print_sensor_data(struct bme280_data *comp_data)
{
    float temp, press, hum;

#ifdef BME280_FLOAT_ENABLE
    temp = comp_data->temperature;
    press = 0.01 * comp_data->pressure;
    hum = comp_data->humidity;
#else
#ifdef BME280_64BIT_ENABLE
    temp = 0.01f * comp_data->temperature;
    press = 0.0001f * comp_data->pressure;
    hum = 1.0f / 1024.0f * comp_data->humidity;
#else
    temp = 0.01f * comp_data->temperature;
    press = 0.01f * comp_data->pressure;
    hum = 1.0f / 1024.0f * comp_data->humidity;
#endif
#endif
    printf("%0.2lf deg C, %0.2lf hPa, %0.2lf%%\n", temp, press, hum);
}

/*!
 * @brief This API reads the sensor temperature, pressure and humidity data in forced mode.
 */
int8_t stream_sensor_data_forced_mode(struct bme280_dev *dev)
{
    /* Variable to define the result */
    int8_t rslt = BME280_OK;

    /* Variable to define the selecting sensors */
    uint8_t settings_sel = 0;

    /* Variable to store minimum wait time between consecutive measurement in force mode */
    uint32_t req_delay;

    /* Structure to get the pressure, temperature and humidity values */
    struct bme280_data comp_data;

    /* Recommended mode of operation: Indoor navigation */
    dev->settings.osr_h = BME280_OVERSAMPLING_1X;
    dev->settings.osr_p = BME280_OVERSAMPLING_16X;
    dev->settings.osr_t = BME280_OVERSAMPLING_2X;
    dev->settings.filter = BME280_FILTER_COEFF_16;

    settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;

    /* Set the sensor settings */
    rslt = bme280_set_sensor_settings(settings_sel, dev);
    if (rslt != BME280_OK)
    {
        fprintf(stderr, "Failed to set sensor settings (code %+d).", rslt);

        return rslt;
    }

    printf("Temperature, Pressure, Humidity\n");

    /*Calculate the minimum delay required between consecutive measurement based upon the sensor enabled
     *  and the oversampling configuration. */
    req_delay = bme280_cal_meas_delay(&dev->settings);

    /* Continuously stream sensor data */
    while (1)
    {
        /* Set the sensor to forced mode */
        rslt = bme280_set_sensor_mode(BME280_FORCED_MODE, dev);
        if (rslt != BME280_OK)
        {
            fprintf(stderr, "Failed to set sensor mode (code %+d).", rslt);
            break;
        }

        /* Wait for the measurement to complete and print data */
        dev->delay_us(req_delay, dev->intf_ptr);
        rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
        if (rslt != BME280_OK)
        {
            fprintf(stderr, "Failed to get sensor data (code %+d).", rslt);
            break;
        }

        print_sensor_data(&comp_data);
    }

    return rslt;
}

void prepare_sensor()
{
	if ((gid.fd = open("/dev/i2c-1", O_RDWR)) < 0) {
		fprintf(stderr, "Failed to open the i2c bus %s\n",
			"/dev/i2c-1");
		exit(1);
	}

	/* Variable to define the result */
	int8_t rslt = BME280_OK;

	/* Make sure to select BME280_I2C_ADDR_PRIM or BME280_I2C_ADDR_SEC as
	 * needed */
	gid.dev_addr = BME280_I2C_ADDR_PRIM;

	if (ioctl(gid.fd, I2C_SLAVE, gid.dev_addr) < 0) {
		fprintf(stderr,
			"Failed to acquire bus access and/or talk to slave.\n");
		exit(1);
	}

	gdev.intf = BME280_I2C_INTF;
	gdev.read = user_i2c_read;
	gdev.write = user_i2c_write;
	gdev.delay_us = user_delay_us;

	/* Update interface pointer with the structure that contains both device
	 * address and file descriptor */
	gdev.intf_ptr = &gid;

	/* Initialize the bme280 */
	rslt = bme280_init(&gdev);
	if (rslt != BME280_OK)
	{
		fprintf(stderr,
			"Failed to initialize the device (code %+d).\n",
			rslt);
		exit(1);
	}
}

int get_temperature(float *external_temperature)
{
	/* Variable to define the result */
	int8_t rslt = BME280_OK;

	/* Variable to define the selecting sensors */
	uint8_t settings_sel = 0;

	/* Variable to store minimum wait time between consecutive measurement in force mode */
	uint32_t req_delay;

	/* Structure to get the pressure, temperature and humidity values */
	struct bme280_data comp_data;

	/* Recommended mode of operation: Indoor navigation */
	gdev.settings.osr_h = BME280_OVERSAMPLING_1X;
	gdev.settings.osr_p = BME280_OVERSAMPLING_16X;
	gdev.settings.osr_t = BME280_OVERSAMPLING_2X;
	gdev.settings.filter = BME280_FILTER_COEFF_16;

	settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;

	/* Set the sensor settings */
	printf("Set the sensor settings\n");
	rslt = bme280_set_sensor_settings(settings_sel, &gdev);
	if (rslt != BME280_OK) {
		fprintf(stderr, "Failed to set sensor settings (code %+d).", rslt);
		return rslt;
	}

	/*Calculate the minimum delay required between consecutive measurement based upon the sensor enabled
	*  and the oversampling configuration. */
	printf("Calculate the minimum delay\n");
	req_delay = bme280_cal_meas_delay(&gdev.settings);
	printf("Delay = %u\n", req_delay);

	/* Set the sensor to forced mode */
	printf("Set the sensor to forced mode\n");
	rslt = bme280_set_sensor_mode(BME280_FORCED_MODE, &gdev);
	if (rslt != BME280_OK) {
		fprintf(stderr, "Failed to set sensor mode (code %+d).", rslt);
		return rslt;
	}

	/* Wait for the measurement to complete and print data */
	printf("Wait for the measurement to complete\n");
	gdev.delay_us(req_delay, gdev.intf_ptr);
	printf("Get data\n");
	rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, &gdev);
	if (rslt != BME280_OK) {
		fprintf(stderr, "Failed to get sensor data (code %+d).", rslt);
		return rslt;
	}

	float temp, press, hum;

#ifdef BME280_FLOAT_ENABLE
	temp = comp_data.temperature;
	press = 0.01 * comp_data.pressure;
	hum = comp_data.humidity;
#else
#ifdef BME280_64BIT_ENABLE
	temp = 0.01f * comp_data.temperature;
	press = 0.0001f * comp_data.pressure;
	hum = 1.0f / 1024.0f * comp_data.humidity;
#else
	temp = 0.01f * comp_data.temperature;
	press = 0.01f * comp_data.pressure;
	hum = 1.0f / 1024.0f * comp_data.humidity;
#endif
#endif

	printf("%0.2lf deg C, %0.2lf hPa, %0.2lf%%\n", temp, press, hum);

	*external_temperature = temp;

	return 0;
}
