/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "time_utils.h"

const long SEC2NSEC = 1000000000L;

void timespec_fix(struct timespec *t)
{
	// fix excess of nanoseconds
	while (t->tv_nsec >= SEC2NSEC) {
		++t->tv_sec;
		t->tv_nsec -= SEC2NSEC;
	}

	// fix excess of negative nanoseconds
	while (t->tv_nsec <= -SEC2NSEC) {
		--t->tv_sec;
		t->tv_nsec += SEC2NSEC;
	}

	// fix negative nanoseconds and positive seconds
	if (t->tv_nsec < 0 && t->tv_sec > 0) {
		--t->tv_sec;
		t->tv_nsec += SEC2NSEC;
	}

	// fix positive nanoseconds and negative seconds
	if (t->tv_nsec > 0 && t->tv_sec < 0) {
		++t->tv_sec;
		t->tv_nsec -= SEC2NSEC;
	}
}

void timespec_diff(struct timespec *a, struct timespec *b,
		struct timespec *result) {
	timespec_fix(a);
	timespec_fix(b);

	result->tv_sec  = a->tv_sec  - b->tv_sec;
	result->tv_nsec = a->tv_nsec - b->tv_nsec;

	timespec_fix(result);
}

void timespec_add(struct timespec *a, struct timespec *b,
		struct timespec *result) {
	timespec_fix(a);
	timespec_fix(b);

	result->tv_sec  = a->tv_sec  + b->tv_sec;
	result->tv_nsec = a->tv_nsec + b->tv_nsec;

	timespec_fix(result);
}

int timespec2ms(struct timespec t)
{
	timespec_fix(&t);
	return ((t.tv_sec) * 1000) + ((t.tv_nsec) / 1000000);
}

int timespec2min(struct timespec t)
{
	timespec_fix(&t);
	return t.tv_sec / 60;
}

struct timespec ms2timespec(int ms)
{
	struct timespec t = {
		.tv_sec = ms / 1000,
		.tv_nsec = (ms % 1000) * 1000000
	};

	timespec_fix(&t);

	return t;
}
