/*
 *  Airfryer - simulate a temperature controlled oven
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Airfryer.
 *
 *  Airfryer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <inttypes.h> // printf

#include "crc16.h"

#include "modbus.h"

const uint8_t STUDENT_ID[] = {8, 4, 6, 2};
const uint8_t STUDENT_ID_0 = STUDENT_ID[0];
const uint8_t STUDENT_ID_1 = STUDENT_ID[1];
const uint8_t STUDENT_ID_2 = STUDENT_ID[2];
const uint8_t STUDENT_ID_3 = STUDENT_ID[3];

int serialise(rtu_message_t *msg, serial_message_t *ser_msg)
{
	if (msg == NULL) {
		fprintf(stderr, "msg is NULL\n");
		return -1;
	}

	if (msg->data_len > DATA_LEN) {
		fprintf(stderr, "Message data too long (%zu > %zu)\n", msg->data_len, (size_t) DATA_LEN);
		return -2;
	}

	size_t index = 0;

	memset(ser_msg->msg, 0, MSG_LEN);

	memcpy(&(ser_msg->msg[index]), &(msg->dev_address), sizeof(msg->dev_address));
	index += sizeof(msg->dev_address);

	memcpy(&(ser_msg->msg[index]), &(msg->fun_code), sizeof(msg->fun_code));
	index += sizeof(msg->fun_code);

	memcpy(&(ser_msg->msg[index]), msg->data, msg->data_len);
	index += msg->data_len;

	msg->crc = calcula_CRC(ser_msg->msg, index);
	memcpy(&(ser_msg->msg[index]), &(msg->crc), sizeof(msg->crc));
	index += sizeof(msg->crc);

	ser_msg->len = index;

	printf("Serialised data: ");

	for (size_t i = 0; i < ser_msg->len; i++) {
		printf("0x%02" PRIX8 " ", ser_msg->msg[i]);
	}

	printf("\n");

	return 0;
}

int deserialise(const serial_message_t ser_msg, rtu_message_t *msg)
{
	if (ser_msg.msg == NULL) {
		fprintf(stderr, "Serial message is NULL\n");
		return -1;
	}

	if (ser_msg.len == 0) {
		fprintf(stderr, "Serial message lenfth is 0\n");
		return -2;
	}

	if (ser_msg.len > MSG_LEN) {
		fprintf(stderr, "Serial message is too long (%zu > %zu)\n", ser_msg.len, (size_t) MSG_LEN);
		return -3;
	}

	printf("Serialised data: ");

	for (size_t i = 0; i < ser_msg.len; i++) {
		printf("0x%02" PRIX8 " ", ser_msg.msg[i]);
	}

	printf("\n");

	if (ser_msg.len < sizeof(uint16_t)) {
		fprintf(stderr, "Message is too short (%zu bytes)\n", ser_msg.len);
		return -4;
	}

	printf("Message has %zu bytes\n", ser_msg.len);
	printf("Checking CRC...\n");

	uint16_t crc = calcula_CRC(ser_msg.msg, ser_msg.len - sizeof(crc));
	memcpy(&(msg->crc), &(ser_msg.msg[ser_msg.len - sizeof(msg->crc)]), sizeof(msg->crc));

	if (msg->crc != crc) {
		fprintf(stderr, "CRC16 mismatch: expected 0x%04" PRIx16 " got 0x%04" PRIx16 "\n", msg->crc, crc);
		return -5;
	}

	size_t index = 0;
	memcpy(&(msg->dev_address), ser_msg.msg, sizeof(msg->dev_address));
	index += sizeof(msg->dev_address);

	memcpy(&(msg->fun_code), &(ser_msg.msg[index]), sizeof(msg->fun_code));
	index += sizeof(msg->fun_code);

	if (ser_msg.len - index > DATA_LEN) {
		fprintf(stderr, "Serial message data too long (%zu > %zu)\n", ser_msg.len, (size_t) DATA_LEN);
		return -6;
	}

	memcpy(msg->data, &(ser_msg.msg[index]), ser_msg.len - index);
	index +=ser_msg.len - index;

	msg->data_len = index;

	return 0;
}

rtu_message_t read_int32(uint8_t dev)
{
	return (rtu_message_t) {
		.dev_address = dev,
		.fun_code = READ,
		.data_len = 1,
		.data = {READ_INT32}
	};
}

rtu_message_t read_float(uint8_t dev)
{
	return (rtu_message_t) {
		.dev_address = dev,
		.fun_code = READ,
		.data_len = 1,
		.data = {READ_FLOAT}
	};
}

rtu_message_t read_string(uint8_t dev)
{
	return (rtu_message_t) {
		.dev_address = dev,
		.fun_code = READ,
		.data_len = 1,
		.data = {READ_STRING}
	};
}

rtu_message_t write_int32(uint8_t dev, int32_t to_send)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = sizeof(to_send) + 1,
		.data = {WRITE_INT32}
	};

	memcpy(&(msg.data[1]), &to_send, sizeof(to_send));

	return msg;
}

rtu_message_t write_float(uint8_t dev, float to_send)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = sizeof(to_send) + 1,
		.data = {WRITE_FLOAT}
	};

	memcpy(&(msg.data[1]), &to_send, sizeof(to_send));

	return msg;
}

rtu_message_t write_string(uint8_t dev, uint8_t *to_send, size_t len)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = len + 2,
		.data = {WRITE_STRING, (uint8_t) len}
	};

	if (len + 2 > DATA_LEN) {
		fprintf(stderr, "Truncating string from %zu to %zu\n", len + 2, (size_t) DATA_LEN);
		len = DATA_LEN;
	}

	printf("Got string '%.*s'\n", (int) len, to_send);
	memcpy(&(msg.data[2]), to_send, len);

	printf("Serialised string: '%.*s'\n", (int) len, &(msg.data[2]));

	return msg;
}

rtu_message_t ask_internal_temperature(uint8_t dev)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = READ,
		.data_len = 1,
		.data = {READ_INTERNAL_TEMP_FLOAT}
	};

	memcpy(&(msg.data[1]), STUDENT_ID, sizeof(STUDENT_ID));
	msg.data_len += sizeof(STUDENT_ID);

	return msg;
}

rtu_message_t ask_reference_temperature(uint8_t dev)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = READ,
		.data_len = 1,
		.data = {READ_REFERENCE_TEMP_FLOAT}
	};

	memcpy(&(msg.data[1]), STUDENT_ID, sizeof(STUDENT_ID));
	msg.data_len += sizeof(STUDENT_ID);

	return msg;
}

rtu_message_t ask_user_commands(uint8_t dev)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = READ,
		.data_len = 1,
		.data = {READ_CMD_INT32}
	};

	memcpy(&(msg.data[1]), STUDENT_ID, sizeof(STUDENT_ID));
	msg.data_len += sizeof(STUDENT_ID);

	return msg;
}

rtu_message_t send_control_signal(uint8_t dev, int32_t signal)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = 1,
		.data = {WRITE_CONTROL_INT32}
	};

	size_t i = msg.data_len;
	memcpy(&(msg.data[i]), STUDENT_ID, sizeof(STUDENT_ID));
	i += sizeof(STUDENT_ID);

	memcpy(&(msg.data[i]), &signal, sizeof(signal));
	i += sizeof(signal);

	msg.data_len = i;

	return msg;
}

rtu_message_t send_reference_signal(uint8_t dev, int32_t signal)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = 1,
		.data = {WRITE_REFERENCE_INT32}
	};

	float fsignal = signal;

	size_t i = msg.data_len;
	memcpy(&(msg.data[i]), STUDENT_ID, sizeof(STUDENT_ID));
	i += sizeof(STUDENT_ID);

	memcpy(&(msg.data[i]), &fsignal, sizeof(fsignal));
	i += sizeof(fsignal);

	msg.data_len = i;

	return msg;
}

rtu_message_t send_on_off_status(uint8_t dev, int32_t status)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = 1,
		.data = {WRITE_ON_OFF_INT32}
	};

	int8_t tsignal = status;

	size_t i = msg.data_len;
	memcpy(&(msg.data[i]), STUDENT_ID, sizeof(STUDENT_ID));
	i += sizeof(STUDENT_ID);

	memcpy(&(msg.data[i]), &tsignal, sizeof(tsignal));
	i += sizeof(tsignal);

	msg.data_len = i;

	return msg;
}

rtu_message_t send_dashboard_terminal_status(uint8_t dev, int32_t status)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = 1,
		.data = {WRITE_TERMINAL_DASHBOARD_INT32}
	};

	int8_t tsignal = status;

	size_t i = msg.data_len;
	memcpy(&(msg.data[i]), STUDENT_ID, sizeof(STUDENT_ID));
	i += sizeof(STUDENT_ID);

	memcpy(&(msg.data[i]), &tsignal, sizeof(tsignal));
	i += sizeof(tsignal);

	msg.data_len = i;

	return msg;
}

rtu_message_t send_start_stop_status(uint8_t dev, int32_t status)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = 1,
		.data = {WRITE_START_STOP_INT32}
	};

	int8_t tsignal = status;

	size_t i = msg.data_len;
	memcpy(&(msg.data[i]), STUDENT_ID, sizeof(STUDENT_ID));
	i += sizeof(STUDENT_ID);

	memcpy(&(msg.data[i]), &tsignal, sizeof(tsignal));
	i += sizeof(tsignal);

	msg.data_len = i;

	return msg;
}

rtu_message_t send_timer(uint8_t dev, int32_t time)
{
	rtu_message_t msg = {
		.dev_address = dev,
		.fun_code = WRITE,
		.data_len = 1,
		.data = {WRITE_TIMER_INT32}
	};

	size_t i = msg.data_len;
	memcpy(&(msg.data[i]), STUDENT_ID, sizeof(STUDENT_ID));
	i += sizeof(STUDENT_ID);

	memcpy(&(msg.data[i]), &time, sizeof(time));
	i += sizeof(time);

	msg.data_len = i;

	return msg;
}

int get_message_data(rtu_message_t msg, extracted_msg_data_t *data)
{
	if (msg.fun_code != WRITE && msg.fun_code != READ) {
		fprintf(stderr, "Not an answer message (code 0x%02" PRIX8 ")\n", msg.fun_code);
		return -1;
	}

	if (msg.data_len == 0) {
		fprintf(stderr, "Empty data\n");
		return -2;
	}

	if (msg.data_len < 2) {
		fprintf(stderr, "Message too short (%zu bytes)\n", msg.data_len);
		return -3;
	}

	memset(data, 0, sizeof(*data));

	switch (msg.data[0]) {
	case WRITE_INT32:
	case READ_INT32:
		data->type = INT32;
		data->len = sizeof(data->data.i32);
		memcpy(&(data->data.i32), &(msg.data[1]), sizeof(data->data.i32));
		break;
	case WRITE_FLOAT:
	case READ_FLOAT:
		data->type = FLOAT;
		data->len = sizeof(data->data.r32);
		memcpy(&(data->data.r32), &(msg.data[1]), sizeof(data->data.r32));
		break;
	case WRITE_STRING:
	case READ_STRING:
		data->type = STRING;
		data->len = msg.data[2];
		memcpy(data->data.str, &(msg.data[2]), sizeof(data->data.str));
		break;
	case READ_INTERNAL_TEMP_FLOAT:
	case READ_REFERENCE_TEMP_FLOAT:
		data->type = FLOAT;
		data->len = sizeof(data->data.r32);
		memcpy(&(data->data.r32), &(msg.data[1]), sizeof(data->data.r32));
		break;
	case READ_CMD_INT32:
		data->type = INT32;
		data->len = sizeof(data->data.i32);
		memcpy(&(data->data.i32), &(msg.data[1]), sizeof(data->data.i32));
		break;
	default:
		fprintf(stderr, "Unknown subcode 0x%02" PRIX8 "\n", msg.data[0]);
		return -4;
	}

	return 0;
}

void print_message_data(extracted_msg_data_t data)
{
	switch (data.type) {
	case INT32:
		printf("Data (int32): %d\n", data.data.i32);
		break;
	case FLOAT:
		printf("Data (float): %f\n", data.data.r32);
		break;
	case STRING:
		printf("Data (string): %.*s\n", (int) data.len, data.data.str);
		break;
	default:
		printf("Data: unknown type %d\n", data.type);
	}
}
