# Airfryer Controller

## Compiling

The compilation was tested on GCC 10.2

This project depends on the WiringPi library

You only need to run `make clean` and `make`

BME280 and the LCD drivers require I2C addresses: `0x76` for the sensor, and
`0x27` for the LCD driver.

## Running

The program will automatically log the outputs and inputs as the CSV file
`airfryer.log`. The point (`.`) is used as the decimal separator.

While executing, the program will prompt the user whether he wants to use UART
only; ignore the UART's temperature and time, using the standard input
(*stdin*), or allowing both the *stdin* and UART to change the variables.

The system reads the standard input on blocking mode. Avoid changing the
temperature or the time while the system is on heating or preheating mode.

## Copyright
- `pid.c`, `pid.h` by Renato Coral Sampaio
- modified `crc16.h` and `crc16.c` by Renato Coral Sampaio
- modified `control_lcd_16x2.c` and `control_lcd_16x2.h` by Lewis Loflin
- BME280 drivers by Bosch Sensortec GmbH under BSD-3-Clause
- all other files are licensed under AGPLv3 by Davi Antônio da Silva Santos